import App from './App.svelte'
import loginSvelte from './views/login.svelte'
const app = new App({
  target: document.getElementById('app')
})

export default app
